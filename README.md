## Project Description

The project takes in a given test cases in which an English or Saurian (Star Trek Language) is given and used to translate into either respective language.

## Running Project

In order to properly run and compile the project you must

1. javac Saurian.java SaurianDriver.java
2. java SaurianDriver.java
