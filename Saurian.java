// Programmer:
// Date:
// The Saurian class has the ability to translate English to Saurian
//  and Saurian to English

public class Saurian {
	// data

	// constants used for translating
	// note M = M and m = m so M and m are not needed
	public static final char[] ENGLISHARR = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	public static final char[] SAURIANARR = { 'U', 'R', 'S', 'T', 'O', 'V', 'W', 'X', 'A', 'Z', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'J', 'K', 'I', 'L', 'N', 'P', 'O', 'Q', 'u', 'r', 's', 't', 'o', 'v', 'w', 'x', 'a', 'z', 'b',
			'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'i', 'l', 'n', 'p', 'o', 'q' };
	public static final int ARRLENGTH = ENGLISHARR.length; // should be the same length for ENGLISHARR and SAURIANARR

	// private variables are set
	private String English;
	private String Saurianarr;

	// default constructor created
	public Saurian() {
		// intalized variables
		English = " ";
		Saurianarr = " ";

	}

	// overload constructor created
	public Saurian(String word, boolean lang) {
		// if language is true set then current language is english
		if (lang == true) {
			English = word;
			translateFromEnglish(word);
		}
		// if not true then word is saurianarr
		else {
			Saurianarr = word;
			translateFromSaurian(word);
		}

	}

	// method to set the current word as english and then translate
	public void setEnglish(String english) {

		this.English = english;
		translateFromEnglish(english);
	}

	// method to set the current word as Saurian then translate it
	public void setSaurian(String saurian) {
		this.Saurianarr = saurian;
		translateFromSaurian(saurian);
	}

	public void translateFromEnglish(String transEng) {
		// Variables intialized to help build word from translatoin
		int numString = transEng.length();
		String toBuild = "";
		int position = 0;

		// for loop that iterates through each character at the current position
		for (int i = 0; i < numString; i++) {
			char Char = transEng.charAt(i);
			for (int j = 0; j < ENGLISHARR.length; j++) {
				// if character is equal to the current english value, then set it equal to the
				// saurinarr equivalent
				if (Char == ENGLISHARR[j]) {
					position = j;
					toBuild += SAURIANARR[position];
					break;
					// otherwise the current character is correct
				} else if (j >= ENGLISHARR.length - 1) {
					toBuild += Char;
				}
			}
		}
		// set translatation to the characters received from for loop
		Saurianarr = toBuild;

	}

	// Swotch Later
	public void translateFromSaurian(String transSau) {
		// Variables intialized to help build word from translatoin
		int numString = transSau.length();
		String toBuild = "";
		int position = 0;

		// for loop that iterates through each character at the current position
		for (int i = 0; i < numString; i++) {
			char Char = transSau.charAt(i);
			for (int j = 0; j < ENGLISHARR.length; j++) {
				// if character is equal to the current Saurinarr value, then set it equal to
				// the
				// English equivalent
				if (Char == SAURIANARR[j]) {
					position = j;
					toBuild += ENGLISHARR[position];
					break;
					// otherwise the current character is correct
				} else if (j >= SAURIANARR.length - 1) {
					toBuild += Char;
				}
			}
		}
		// set translatation to the characters received from for loop
		English = toBuild;
	}

	// get the current english variable value
	public String getEnglish() {
		return English;
	}

	// get the current Saurian variable value
	public String getSaurian() {
		return Saurianarr;
	}

}
